# Synopsis

This is a latex beamer theme for the creation of presentation slides in context
of the FORMUS³IC project.

# History

This document is a customized fork of the OTH Regensburg beamer theme. In a
first step is has been customized for project internal meetings. In a second
step, there was lots of refactoring work done.

# Motivation

A Microsoft PowerPoint template for presentations has been around since the very
beginning of the FORMUS³IC project. However, people prefering Latex for the
creation of presentations (or people using GNU/Linux, where access to Microsoft
Powerpoint is complicated), had no equivalent template.

This template tries to be as close as possible to the Microsoft PowerPoint
template.

# Installation

## Linux

On Linux based systems, the theme can be used by extending the `TEXINPUTS`
system variable:
```bash
export TEXINPUTS=$TEXINPUTS:<path-to-theme-directory>
```

Another approach is to put the theme directory into the following directory:
```bash
~/texmf/tex/themes/<contents-of-theme-directory>
```
## Windows

Sorry, currently there is no such guide, please write me if you have
instructions :).

# Contributors

Bug fixes are alwasy welcome, please fork the project and send merge requests /
patches via mail.
