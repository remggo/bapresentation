(TeX-add-style-hook
 "beamerthemeFORMUSIC"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("textpos" "absolute" "overlay")))
   (TeX-run-style-hooks
    "scrextend"
    "theme/FORMUSIC_colors"
    "theme/FORMUSIC_fonts"
    "theme/FORMUSIC_logos"
    "textpos"
    "pdftexcmds"
    "caption"
    "etoolbox")
   (TeX-add-symbols
    '("setlistspacing" 2)
    "olditem"))
 :latex)

