	std::future<int> f1 = async([&]{
		calculate_ultimate_answer();
		return 42;
	});
	
	std::future<long> f2 = f1.then([&](std::future<int> f){
		tell_answer(f.get()); // not blocking -> is ready
		return calc_amazed_people();		
	})
	
	auto amazed_people_count = f2.get();