void setqDot(PSystem *sys, float *q, float *qDot, int factor) {
    using embb::algorithms::Invoke;
    Invoke([&] {
               ThpipeN_qDot(&sys->pipe1_2, q, qDot);
               // ...
               ThpipeN_qDot(&sys->pipe6_3, q, qDot);
           },
           // two additional lambdas with similar characteristics
           [&] {
               ThpipeN_qDot(&sys->pipe4, q, qDot);
               // ...
               ThpipeN_qDot(&sys->pipe5, q, qDot);
           }
    );
}
