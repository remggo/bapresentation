	std::for_each(it.begin(), it.end(), [&](auto val){
		process(val);
	});
	// Parallel Equivalent
	std::for_each(std::par, it.begin(), it.end(), [&](auto val){
		process(val);
	});
