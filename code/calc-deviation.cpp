template <typename Function, typename ExecutionPolicy>
inline void calculate_deviation(const Function &function, std::vector<double> &deviation, ExecutionPolicy &policy) {
    hpx::parallel::for_loop(policy, 3, function.size() - 3, [&](int i) {
        deviation[i] = double(function[i + 3] - function[i - 3]) / 6.0;
    });
}
