// Calculates $\int_{a}^{b} f(u)\,{\rm d}u \approx \frac{1}{N}\,\sum_{i=1}^N f(x_i)$
template <typename ExecutionPolicy>
inline double calc_integration(const int a, const int b, const std::vector<double> &data,ExecutionPolicy &policy) {
    double factor = double(b - a) / double(b - a + 1);
    double sum = hpx::parallel::reduce(hpx::parallel::seq, data.begin() + a, data.begin() + b, (double)0.0);

    return (factor * sum);
}
