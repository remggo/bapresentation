template <typename Executor_, typename F, typename... Ts>
static auto async_execute(Executor_ &&exec, F &&f, Ts &&... ts)
// ...
{
    return exec.async_execute(std::forward<F>(f), std::forward<Ts>(ts)...);
}
