hpx::future<RandomIt> parallel_sort_async(ExPolicy && policy, RandomIt first, RandomIt last, Compare comp) {
// ...   
  result = executor_traits::async_execute(
      policy.executor(), &sort_thread<ExPolicy, RandomIt, Compare>, std::ref(policy), first, last, comp);
// ...
  return result;
}
