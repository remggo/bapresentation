	std::future<result_t> dgl1 = async([&]{
		calculate_dgl1();
	});
	auto dgl2 = calculate_dgl2();
	// .get() blocking call until ready
	calculate(dgl1.get(), dgl2); 
