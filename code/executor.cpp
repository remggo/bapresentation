	// NEW: permitting parallel execution in the current thread only
	sort(this_thread::par, data.begin(), data.end());
	// NEW: permitting vector execution in the current thread only
	sort(this_thread::vec, data.begin(), data.end());
	// NEW: permitting parallel execution on a user-defined executor
	my_executor my_exec = ...;
	std::sort(par.on(my_exec), data.begin(), data.end());
