$ xeno-config --posix --cflags
-I/usr/xenomai/include/cobalt -I/usr/xenomai/include -D_GNU_SOURCE -D_REENTRANT -D__COBALT__ -D__COBALT_WRAP__
$ xeno-config --posix --ldflags
-Wl,@/usr/xenomai/lib/cobalt.wrappers   /usr/xenomai/lib/xenomai/bootstrap.o -Wl,--wrap=main -Wl,--dynamic-list=/usr/xenomai/lib/dynlist.ld -L/usr/xenomai/lib -lcobalt -lpthread -lrt   

