// Calculates $\int_{a}^{b} f(u)\,{\rm d}u \approx \frac{1}{N}\,\sum_{i=1}^N f(x_i)$
inline double calc_integration(const int a, const int b,
                               const std::vector<double> &data) {
    double factor = double(b - a) / double(b - a + 1);
    double sum = std::accumulate(data.begin() + a, data.begin() + b, 0.0);
    return (factor * sum);
}
