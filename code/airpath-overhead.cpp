hpx::performance_counters::performance_counter cum_overhead( "/threads{locality#0/worker-thread#0}/time/cumulative-overhead");
hpx::performance_counters::performance_counter cum_time( "/threads{locality#0/worker-thread#0}/time/cumulative");
cum_overhead.reset();
cum_time.reset();
// Execute one Iteration
run(&sys);
// End of Iteration
auto overhead = cum_overhead.get_counter_value(hpx::launch::async);
auto time = cum_time.get_counter_value(hpx::launch::async);
