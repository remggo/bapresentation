std::vector<int> v = ...;
// sort with dynamically-selected execution
size_t threshold = ...;
execution_policy exec = seq;
if (v.size() > threshold){
	exec = par;
}
sort(exec, v.begin(), v.end());