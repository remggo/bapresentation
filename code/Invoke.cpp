Invoke(
    [&] {
        // Calculate begin and end for loop
        for (int i = begin + 3; i < end - 3; i++) {
            deviation[i] = double(function[i + 3] - function[i - 3]) / 6.0;
        }
    },
    [&] {
        // Calculate begin and end accordingly
        for (int i = begin + 3; i < end - 3; i++) {
            deviation[i] = double(function[i + 3] - function[i - 3]) / 6.0;
        }
    });
