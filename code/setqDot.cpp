void setqDot(PSystem *sys, float *q, float *qDot, int factor) {
    std::vector<hpx::future<void>> futures(4);
    futures[0] = hpx::async([&] {
            ThpipeN_qDot(&(sys->pipe1_2), q, qDot);
            // ...
            ThpipeN_qDot(&(sys->pipe6_3), q, qDot);
            });
    // ... similar for futures[1] and [2]
    futures[3] = hpx::async([&] {
            ThpipeN_qDot(&(sys->pipe2_1), q, qDot);
            // ...
            ThpipeN_qDot(&(sys->pipe5), q, qDot);
            });
    auto res = hpx::when_all(futures.begin(), futures.end());
    res.get();
}
