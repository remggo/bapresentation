first_deviation = calculate_deviation(curve);
second_deviation = calculate_deviation(first_deviation);
roots = calculate_roots(second_deviation);
integrations = integrate_over_roots(second_deviation, roots);
result = find_second_biggest_negative_area(integrations);
